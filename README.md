Workshop Get Started With MLOps
===============================

This repo is a backup of the workshop notes on iffMD.

- [Workshop Get Started With MLOps](https://iffmd.fz-juelich.de/cBVSp52ySMaMnZ4NkAq-MA#)
- [Workshop development notes](https://iffmd.fz-juelich.de/AMgRvI13QA-btd08sqBH3Q#)

Note that proper viewing of the Markdown notes requires support for CodiMD / HackMD.io syntax, as is provided by iffMD.fz-juelich.de.

## Repo mirrors

[Main repo](https://iffgit.fz-juelich.de/phd-project-wasmer/teaching/workshop-get-started-with-mlops). Pull mirrors: [1](https://jugit.fz-juelich.de/hds-lee/2023-retreat/workshop-get-started-with-mlops), [2](https://codebase.helmholtz.cloud/hds-lee/2023-retreat/workshop-get-started-with-mlops). Last sync: 2023-09-19.
