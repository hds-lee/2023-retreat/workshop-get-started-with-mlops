Workshop Get Started With MLOps
===============================

<img src="https://iffmd.fz-juelich.de/uploads/upload_c6088b751265dcc422343be079e17cda.png" alt="ml-ops.org logo" width="400"/>

*Figure reference: https://ml-ops.org/.*


###### tags: `workshop`, `Helmholtz`, `HDS-LEE`, `data science`, `AI`, `MLOps`, `RDM`, `software tools`, `DVC`

# Description

<!-- DEVELOPER'S NOTE. Link to workshop dev repo & dev notes see 'Further Reading' at the end. -->

**Workshop Title.** Get Started With MLOps.

**Authors.** [R. Hilgers](https://www.fz-juelich.de/profile/hilgers_r) [<img src="https://raw.githubusercontent.com/FortAwesome/Font-Awesome/6.x/svgs/brands/github.svg" height="17">](https://github.com/RobinHilg), [J. Wasmer](https://www.fz-juelich.de/profile/wasmer_j) [<img src="https://raw.githubusercontent.com/FortAwesome/Font-Awesome/6.x/svgs/brands/github.svg" height="17">](https://github.com/Irratzo) [<img src="https://raw.githubusercontent.com/FortAwesome/Font-Awesome/6.x/svgs/brands/x-twitter.svg" height="17">](https://twitter.com/JohannesWasmer).

**Part of event.** The [HDS-LEE Retreat 2023](https://www.hds-lee.de/events/hds-lee-retreat-2023/), 2023-08-15.

**Abstract.** Training and evaluating a large number of machine learning models and sequentially obtained data requires automated pipelines, which are part of what is commonly referred to as MLOps. We will give a 15 minute introduction to the topic. In the remaining time, beginners will receive "Get Started" and reference material as well as assistance. Advanced attendees, meanwhile, can bring their own projects and team up in pairs, by interest or random choice, to get insight into each other's project in a "Pair Programming" style.

# Table of Contents

[TOC]

# Introduction

## What is MLOps?

> **MLOps** (Machine Learning Operations) is a paradigm, including aspects like [...] end-to-end conceptualization, implementation, monitoring, deployment, and scalability of machine learning products. [...] contributing disciplines: machine learning, software engineering (especially DevOps), and data engineering. MLOps is aimed at productionizing machine learning systems by bridging the gap between development (Dev) and operations (Ops). [...] leveraging [...] [the] principles [of]: CI/CD automation, workflow orchestration, reproducibility; versioning of data, model, and code; collaboration; continuous ML training and evaluation; ML metadata tracking and logging; continuous monitoring; and feedback loops.

*Quote reference: [Kreuzberger et al.](https://doi.org/10.1109/ACCESS.2023.3262138). Machine Learning Operations (MLOps): Overview, Definition, and Architecture. IEEE Access 11, 31866–31879 (2023).*

<img src="https://iffmd.fz-juelich.de/uploads/upload_bbf7b72c1b2966f28f705f3d434c1a45.png" alt="The MLOps lifecycle" width="800"/>

*The MLOps lifecycle. Figure reference: Neptune AI Blog - ML Experiment Tracking. [URL](https://neptune.ai/blog/ml-experiment-tracking).*

MLOps originates from the **DevOps** methodology in software engineering. DevOps aims to reduce the time required to develop deployable software by relying on continous integration and continous delivery (CI/CD) and automated pipelines. A CI/CD pipeline can be used for anything, from automatic code checking on every commit, to running full ML pipelines on the respective Git remote server instances with automatic logging. [DevOps in practice](https://www.youtube.com/watch?v=scEDHsr3APg) is nowadays most often performed either with [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) or [GitHub Actions](https://github.com/features/actions). For instance, all Helmholtz self-hosted code repositories are GitLab instances. many research groups host on or mirror their projects to GitHub to increase their visibility. Take note, though, that GitHub Actions are only free for public repositories.

In DevOps, pipelines build, test and deploy code, while in MLOps pipelines train, evaluate, and deploy models to production. Pipelines are also used in MLOps for data management, including the automation of Extract, Transform, Load (ETL) procedures and adding data to a repository.

CI/CD tools, in turn, are merely the infrastructure for modern practices of building software, often called **[agile software development](https://www.google.com/search?q=agile+software+development)**. In agile development, a *complex* and therefore not fully plannable product such as a piece of software is not planned once in the beginning and released in working state once it is done, but continuously reorientied and released in working state throughout its development. [Scrum](https://www.google.com/search?q=scrum+in+a+nutshell) is a popular one of these agile practices, for instance. But, beware of [certified agile coaches](https://www.youtube.com/watch?v=Bez7wmAsxjE) :wink:.

## Why you should care about MLOps

This addresses data science researchers.

**Automation**

- Track data changes and addition
  - Sequentially acquired data
  - ETL from multiple data sources
  - Retrain models based on added data
- Track model's performances
- Track experiments and model changes
- Running production model
- Update/change production model as an experiment outperforms it
- Generate plots of KPI's

**Collaboration**

- Transparency on model performance changes
- 24/7 availability of most recent results
- Collaborative model development
- ETL data compilation from databases only available to one  collaborator

**Publication**

- Transparent historic model and data development and performance for reviewers
- Referencing of unique model hashes in publications

**Research data management (RDM)**
- FAIR Data Principles (DOI: 10.1038/sdata.2016.18)
  - Findable
  - Accessible
  - Interoperability
  - Reusability
- Concerns of your research group
  - Archival of PhD projects
- Research data policy of your employer
  - [FZJ RDM Guidelines](https://www.fz-juelich.de/en/zb/open-science/research-data-management)
  - [RWTH RDM Guidelines](https://www.rwth-aachen.de/cms/root/Forschung/Forschungsdatenmanagement/~ncfw/Leitlinie-zum-Forschungsdatenmanagement/lidx/1/)
  - [UoC RDM Guidelines](https://fdm.uni-koeln.de/en/rdm-guideline)

**Career outside academia**

:::info
:thinking_face: **Collect 20 random positions for "Data Scientist" on e.g. LinkedIn. Count how many of them include the keywords MLOps, Databricks, MLflow, Azure, data pipelines, and agile methods.**
:::

## When you should *not* care about MLOps

In the business sector, most data-centric medium to large companies use MLOps techniques and tools. Often, such tools are designed for teams of 5-50 people and not for a single-person project.

<img src="https://iffmd.fz-juelich.de/uploads/upload_d853bcce2c1545d8bcc2d25e231d3e16.gif" alt="The MLOps lifecycle" width="600"/>

*End-to-end MLOps architecture. Figure reference: [Kreuzberger et al.](https://doi.org/10.1109/ACCESS.2023.3262138). Machine Learning Operations (MLOps): Overview, Definition, and Architecture. IEEE Access 11, 31866–31879 (2023).*

Within academia, therefore, MLOps methods and tools are not widely used. This is partly due to the often smaller project scale ("One grad student, one model"), partly due to the more "offline" nature of scientific ML applications and data (models are developed for a publication and not for continuous operation).

Hence, the initial setup cost for an MLOps enviroment including pipelines etc. can represent an entrance barrier for individual researchers to be taken into account.

:::info
:thinking_face: **Start by asking yourself which parts of the MLOps lifecycle you actually need, and if the setup investment is worth it.**
:::


# DVC - Data Version Control

Let's pick one representative and popular MLOps tool. We'll pick **[DVC ](https://dvc.org/)**.

What MLOps problems does it address and how?

<img src="https://iffmd.fz-juelich.de/uploads/upload_6e8493b5203aa53667cc69d0f47c8dc0.png" alt="dvc" width="100"/>

*DVC logo. Figure reference: Iterative AI. [URL](https://iterative.ai/).*

## What is DVC?

**DVC is ...**
- ... built on top of Git.
- ... a tool used to track and compare data, models and experiments.
- ... designed to be able to handle large data volumes.


**DVC can be used for ...**
- ... integrating externally hosted data into ML and data pipelines within GitHub's or GitLab's CI/CD.
- ... monitoring experiment and model performance across branches and versions.
- ... referencing a specific version of a model or dataset.

## DVC core components

The motivating example is transforming a binary Pokemon CNN classifier, coded as a working prototype in a Jupyter notebook, into an DVC pipeline. Based on a set of attributes, the CNN predicts whether a Pokemon is of "water" type or not (Tutorial: [repository](https://github.com/RCdeWit/dtc-workshop), [video recording](https://www.youtube.com/watch?v=6x6GwtNeYdI), [blog article](https://iterative.ai/blog/jupyter-notebook-dvc-pipeline).)

<img src="https://iffmd.fz-juelich.de/uploads/upload_14d88805b1fccc6cfe6b1134049d4d36.png" alt="A Pokemon" width="300"/>

*A Pokemon. Figure reference: Rob de Wit, Pokémon images (001--905). [URL](https://www.kaggle.com/datasets/robdewit/pokemon-images).*

:::danger
**External change** :fire: New Pokemon games were released recently.
:::

Consequences of this scenario include:

- Altered dataset
- Probably model drift
- Changed model performance
- Model may require retraining

The decribed consequences require an update to the prototype model. The appropriate update is determined by **ML experimentation**. However, different models and parameter combinations, result in a large number of experiments.
This leaves us with the problem: *How to keep track, while maintaining reproducibility?*

We define an experiment as the combination of

- Data,
- Code, and
- Parameters.

Keeping track of the different experiments and the correspondig performances and KPI's requires **Version control**.

Version control is commonly known for code development by the implementation of Git. However, transferring the concept of version control to MLOps, two parallel version control frameworks are used in parallel:

- Code, parameters: Git
- Data, models: DVC

While the implementation of Git is based on the ``` diff ``` of the individual file, models (and sometimes data) are typically stored as binary files, which makes ```diff``` unefficient. Therefore, a handling of both models as well as large (potential binary) data files with Git is undersireable.

DVC ties the data versioning to the corresponding Git commit history. DVC has three main features.

- Data version control
- Set up and run automated Pipelines
- Tracking Experiments

**DVC data version control.**

Instead of committing the data to Git, DVC commits the data's metadata `dataset.dvc` (hash, size, nfiles, ...). This `.dvc` file points to something in the `.dvc/cache`. DVC can resolve the specific data files in the remote storage (by default, the local computer; cloud storages get duplicated locally, or sth) via reflinks. If a new commit changes the `dataset.dvc`, it can differentially point to sth else in the cache. For instance, some images in the training data folder were rmoved, and some added. This avoids data duplication over incremental changes.

<img src="https://iffmd.fz-juelich.de/uploads/upload_1977067023514820985f08f0b35dffe5.png" alt="ML pipeline stages" width="700"/>

*DVC tracks incremental changes to data. Figure reference: [GitOps for ML Tutorial - video recording](https://www.youtube.com/watch?v=6x6GwtNeYdI&t=8m15s).*

**DVC pipelines.**

DVC pipelines are directed acyclic graphs (DAGs) of connected steps or stages. For instance, data preprocessing, loading, model training, performance evaluation. Each stage has inputs and outputs. This makes it possible to control stage execution via DVC. For instance only start data loading once the dataset labels and images from preprocessing are stored in DVC cache. This makes pipelines reliable and reproducible. DVC pipelines are described as YAML files `dvc.yaml`.

<img src="https://iffmd.fz-juelich.de/uploads/upload_8623aa24ea72a7bab1d2914c7c0189e4.png" alt="ML pipeline stages" width="500"/>

*Stages in a typical ML pipeline, data I/O on the right. Figure reference: [GitOps for ML Tutorial - video recording](https://www.youtube.com/watch?v=6x6GwtNeYdI&t=9m45s).*

**DVC experiments.**

DVC pipelines enable experiments. A `dvc.yaml` pipeline has inputs code, data, parameters, and outputs model, plots, metrics. Version control:

- Git: Code, parameters, pipeline, metrics.
- DVC: Data, model, plots.

**A set of specific pipeline, inputs and outputs constitute one experiment = one Git commit.** Via version control, we can return to any experiment and reproduce it if needed.

<img src="https://iffmd.fz-juelich.de/uploads/upload_c43b950b357311358a9e0072434bd24a.png" alt="ML pipeline stages" width="700"/>

*A DVC experiment, represented by a Git commit. Figure reference: [GitOps for ML Tutorial - video recording](https://www.youtube.com/watch?v=6x6GwtNeYdI&t=11m25s).*


# Comparing MLOps tools

MLOps tools cover a subset of the MLOps tools lifecycle.

Let's pick six popular, representative tools from the large MLOps tools ecosystem and compare them. We exclude the topics hyperparameter tuning and model serving in this selection.

- <img src="https://iffmd.fz-juelich.de/uploads/upload_6e8493b5203aa53667cc69d0f47c8dc0.png" height="30"> **[DVC](https://dvc.org)**
- <img src="https://hydra.cc/img/logo.svg" height="30"> **[Hydra](https://hydra.cc)**
- <img src="https://raw.githubusercontent.com/wandb/assets/main/wandb-dots-logo.svg" height="30"> **[Wandb](https://wandb.ai)**
- <img src="https://raw.githubusercontent.com/mlflow/mlflow/master/assets/icon.svg" height="30"> **[MLflow](https://mlflow.org)**
- <img src="https://www.datalad.org/img/logo/datalad_logo_solo.svg" height="30"> **[DataLad](https://datalad.org)**
- <img src="data:image/svg+xml;base64,PHN2ZyBmaWxsPSIjRkZDOTAwIiByb2xlPSJpbWciIHZpZXdCb3g9IjAgMCAyNCAyNCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+S2Vkcm88L3RpdGxlPjxwYXRoIGQ9Im0xMiAwIDEyIDEyLTEyIDEyTDAgMTIgMTIgMHoiLz48L3N2Zz4=" height="30"> **[Kedro](https://kedro.org/)**

General notes:

- Many MLOps tools are provided by startups. For instance, DVC is provided by Iterative. Typically, the tool is open source and free to use for individual needs. The companies revenue, meanwhile, comes from paid offerings for business needs such as security and compliance features, who spent the most computational resources, who has access to this restricted data, etc.
- MLOps tools can be divided into two categories: **Platforms** and **ecosystem tools**. Platform tools provide a comprehensive solution for the whole MLOps lifecycle, such as AWS SageMaker. This often is tied to tool-specific storages. Ecosystem tools, on the other hand, are tools that focus on solving only one or a subset of stages in the whole lifecycle, but usually better than a full MLOps product.
- For instance, use DVC for data versioning and Wandb in the cloud or MLflow self-hosted for experiment tracking. This depends on the use case, preference, and budget.

## Wandb

<img src="https://raw.githubusercontent.com/wandb/assets/main/wandb-dots-logo.svg" height="30"> **[Wandb](https://wandb.ai)** (aka Weights & Biases) and MLflow are examples of **experiment tracking** tools, mostly online metrics and experimentation history. This is important during model development. Tools like DVC and DataLad, instead, capture the end result of experiments and workflows, to present to the team or deployment. In the free version, Wandb is not self-hosted. This make it easier to start using than MLflow, but one has to consider this in case of sensitive data.

## MLflow

<img src="https://raw.githubusercontent.com/mlflow/mlflow/master/assets/icon.svg" height="30"> **[MLflow](https://mlflow.org)** is an example of a **platform tool** that provides modules for data versioning, experiment tracking, model serving and management. Unlike DVC whose versioning builds on top of Git, most other tools like MLflow that offer versioning use their own kind of DB and API to relize that.

## Hydra

<img src="https://hydra.cc/img/logo.svg" height="30"> **[Hydra](https://hydra.cc)** is used to create elegantly configured applications. It is growing in popularity among ML researchers, because it helps you design & configure projects like ML experiments, based on simple YAML config files. Experiments are stored in a time-stamped directory. This is the equivalent to DVC's commits that refer to a single experiment. Experiments with specific input settings can be easily reproduced given the respective config file.

## DataLad

<img src="https://www.datalad.org/img/logo/datalad_logo_solo.svg" height="30"> **[DataLad](https://datalad.org)** is like DVC, but more specifically designed for reproducible research. For instance, you can define a pipeline that reproduces published results starting from the raw inputs. It is co-developed by [FZJ INM-7](https://www.fz-juelich.de/de/inm/inm-7/leistungen/tools/datalad). Unlike DVC which separates code & data storage, DataLad stores & tracks data & code in one place, based on the Git large file extension git-annex.

## Kedro

<!-- Note. Source of Kedro logo SVG code is from GitHub Kedro README Kedro badge, 2024-02. -->

<img src="data:image/svg+xml;base64,PHN2ZyBmaWxsPSIjRkZDOTAwIiByb2xlPSJpbWciIHZpZXdCb3g9IjAgMCAyNCAyNCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+S2Vkcm88L3RpdGxlPjxwYXRoIGQ9Im0xMiAwIDEyIDEyLTEyIDEyTDAgMTIgMTIgMHoiLz48L3N2Zz4=" height="30"> **[Kedro](https://kedro.org/)** is developed by McKinsey's "Quantum Black, AI" division to solve the problem of "production-ready data science". Like MLflow, it is part of the Linux Foundation of open source data & AI tools. Its focus is on software engineering best practices, data pipelines and experiment tracking. It integrates with popular tools and cloud platforms like Docker, Jupyter and MLflow.

# Hands-On

The remainder of the workshop time is reserved for you to just try out one of these tools. We provide links to "Get Started" guides here for DVC, Hydra, Wandb, MLflow and DataLad. Other suggestions can be found in the [Further reading](#Further-reading) section at the end.

:::success
<img src="https://iffmd.fz-juelich.de/uploads/upload_6e8493b5203aa53667cc69d0f47c8dc0.png" height="30"> **[Get Started With DVC](https://dvc.org/doc/start)**
:::

:::spoiler DVC Tips & Resources
- The [DVC Extension for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=Iterative.dvc) allows to "Run, compare, visualize, and track machine learning experiments right in VS Code".
- Just like Git needs a remote for code, DVC requires a remote for data storage. [DVC remote storage options](https://dvc.org/doc/user-guide/data-management/remote-storage) lists the supported options. In general, initial setup of DVC remote storage can be a bit fiddly.
  - Possibly, your institute provides access to a regularly backed up partition for data with large enough quota. It might be called `/Data/.../$USER/`, for instance. In that case, we recommend the SSH remote storage option.
  - If you choose a cloud option, the company DagsHub claim they can automate this setup in a few lines of code. Their service is free for individuals with 100 GB storage. See [DagsHub set up DVC remote storage](https://www.google.com/search?&q=dagshub+set+up+dvc+remote+storage).
- The tutorial "GitOps for ML - How to convert machine learning notebooks to reproducible DVC pipelines" is a recipe for turning existing, Jupyter Notebook - based ML project into DVC pipelines. Tutorial [repository](https://github.com/RCdeWit/dtc-workshop), [video recording](https://www.youtube.com/watch?v=6x6GwtNeYdI), [blog article](https://iterative.ai/blog/jupyter-notebook-dvc-pipeline).
- DVC supports [integration of DVC and Hydra](https://iterative.ai/blog/dvc-hydra-integration). This combines both tools' respective strengths in version control and project configuration.
- [Awesome Iterative Projects](https://github.com/iterative/awesome-iterative-projects) is an "[awesome list](https://github.com/sindresorhus/awesome)" of projects successfully using DVC & related tools.
- There is another startup called [DagsHub](https://dagshub.com/docs/index.html) that [says](https://dagshub.com/docs/faq/#q-so-why-not-just-use-git-and-dvc-through-the-command-line) about itself: "DagsHub is for DVC what GitHub is for git". It visualizes the pipeline, integrates with MLflow and others. Public repo usage [is free](https://dagshub.com/pricing), as it is for GitHub. [Here](https://dagshub.com/khuyentran1401) are some projects combining several of the MLOps tools mentioned here, courtesy of [Khuyen Tran](https://github.com/khuyentran1401).
- If you prefer to do everything from within Python, the tool [ZnTrack](https://github.com/zincware/ZnTrack) claims to be an "Object-Relational Mapping for DVC".
  - 2023-08 ZnTrack tutorial. [YouTube video](https://www.youtube.com/watch?v=7ZgBydEPHwA), [repo](https://github.com/PythonFZ/DVCExample/tree/mnist_sign_language), [slides](https://onedrive.live.com/view.aspx?resid=7FED1240F77A0670!2402&ithint=file%2cpptx&authkey=!ALoB14yh4e7i80w), [Binder notebook](https://notebooks.gesis.org/binder/jupyter/user/pythonfz-dvcexample-vjjh1ft8/doc/tree/Workflow.ipynb).
:::

-----

:::success
<img src="https://hydra.cc/img/logo.svg" height="30"> **[Get Started With Hydra](https://hydra.cc/docs/tutorials/basic/your_first_app/simple_cli/)**
:::

:::spoiler Hydra Tips & Resources
- [Stop Hard Coding in a Data Science Project – Use Config Files Instead](https://mathdatasimplified.com/2023/05/25/stop-hard-coding-in-a-data-science-project-use-configuration-files-instead/)
- If you think that the official Hydra docs are a bit plain, try [Complete tutorial on how to use Hydra in Machine Learning projects](https://kushajveersingh.com/blog/complete-tutorial-on-how-to-use-hydra-in-machine-learning-projects) instead (or [these](https://danmackinlay.name/notebook/hydra_ml.html#tutorials-and-examples)).
- DVC supports [integration of DVC and Hydra](https://iterative.ai/blog/dvc-hydra-integration). This combines both tools' respective strengths in version control and project configuration.
- If you prefer to do everything from within Python, the tool [Hydra-Zen](hhttps://github.com/mit-ll-responsible-ai/hydra-zen) claims to "eliminate all hand-written yaml configs from your Hydra project".
:::

-----

:::success
<img src="https://raw.githubusercontent.com/wandb/assets/main/wandb-dots-logo.svg" height="30"> **[Get Started With Wandb](https://docs.wandb.ai/quickstart)**
:::

:::spoiler Wandb Tips & Resources
- None.
:::

-----

:::success
<img src="https://raw.githubusercontent.com/mlflow/mlflow/master/assets/icon.svg" height="30"> **[Get Started With MLflow](https://mlflow.org/docs/latest/quickstart.html)**
:::

:::spoiler MLflow Tips & Resources
- There is another startup called [DagsHub](https://dagshub.com/docs/index.html) that [says](https://dagshub.com/docs/faq/#q-so-why-not-just-use-git-and-dvc-through-the-command-line) about itself: "DagsHub is for DVC what GitHub is for git". It visualizes the pipeline, integrates with MLflow and others. Public repo usage [is free](https://dagshub.com/pricing), as it is for GitHub. [Here](https://dagshub.com/khuyentran1401) are some projects combining several of the MLOps tools mentioned here, courtesy of [Khuyen Tran](https://github.com/khuyentran1401).
:::


-----

:::success
<img src="https://www.datalad.org/img/logo/datalad_logo_solo.svg" height="30"> **[Get Started With DataLad](https://handbook.datalad.org/en/latest/intro/user_types.html)**
:::

:::spoiler DataLad Tips & Resources
- None.
:::

-----

:::success
<img src="data:image/svg+xml;base64,PHN2ZyBmaWxsPSIjRkZDOTAwIiByb2xlPSJpbWciIHZpZXdCb3g9IjAgMCAyNCAyNCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48dGl0bGU+S2Vkcm88L3RpdGxlPjxwYXRoIGQ9Im0xMiAwIDEyIDEyLTEyIDEyTDAgMTIgMTIgMHoiLz48L3N2Zz4=" height="30"> **[Get Started With Kedro](https://kedro.org/#get-started)**
:::

:::spoiler Kedro Tips & Resources
- [Awesome Kedro](https://github.com/kedro-org/awesome-kedro). Plugins, extensions, case studies, articles, and video tutorials for Kedro. :::


# Further reading

- Related workshops.
  - Course "Data Pipelines for Science" by the [Accelerate Science](https://acceleratescience.github.io/) programme of U Cambridge. [Course homepage](https://acceleratescience.github.io/data-engineering-school), [course repo](https://github.com/acceleratescience/data-school), [resources](https://acceleratescience.github.io//resources). No MLOps tools used, but includes nice code publication template.
- Start-up blogs.
  - Neptune AI blog, "MLOps Landscape in 2023: Top Tools and Platforms", 2023. [URL](https://neptune.ai/blog/mlops-tools-platforms-landscape).
  - DagsHub blog, "How to Compare ML Experiment Tracking Tools to Fit Your Data Science Workflow", 2021. [URL](https://dagshub.com/blog/how-to-compare-ml-experiment-tracking-tools-to-fit-your-data-science-workflow/).
  - Neptune AI blog, "How to Build an End-To-End ML Pipeline", 2023. [URL](https://neptune.ai/blog/building-end-to-end-ml-pipeline).
- GitHub.
  - GitHub, "Awesome MLOps - A curated list of references for MLOps". [URL](https://github.com/visenger/awesome-mlops).
  - GitHub, Topics, LLMOps. [URL](https://github.com/topics/llmops).
- Podcasts on Python, software engineering, MLOps.
- Workshop development
  - [Workshop repository](https://iffgit.fz-juelich.de/phd-project-wasmer/teaching/workshop-get-started-with-mlops)
  - [Workshop development notes](https://iffmd.fz-juelich.de/AMgRvI13QA-btd08sqBH3Q)
