Workshop Get Started With MLOps - Development Notes
==================================================

###### tags: `workshop`, `ML`, `data versioning`, `DVC`, `HDS-LEE`

# Description

Development notes for "Workshop Get Started With MLOps"

- [Workshop](https://iffmd.fz-juelich.de/cBVSp52ySMaMnZ4NkAq-MA)
- [Workshop repository](https://iffgit.fz-juelich.de/phd-project-wasmer/teaching/workshop-get-started-with-mlops)

# Table of Contents

[TOC]

# iffMD building blocks

For copy-paste into published workshop notes.

[iffMD features](https://iffmd.fz-juelich.de/features#) and [HackMD features](https://hackmd.io/s/features).

<span style="color:red">**Temporary note in red.**</span>

:::info
**Info** :mega: Some info.
:::

:::success
**Task** :first_place_medal: Some task.
:::

:::warning
**Caution** :zap: Some caution.
:::

:::danger
**Warning** :fire: Some warning.
:::

:::spoiler Hidden text.

For example, a solution or auxilliary material.
:::

# Johannes notes

## Brainstorming

- The REAL idea for the workshop is \"automation\". That\'s what MLOps
  and pipelines are all about.

- Outline: two parts.
  - Part 1: Intro.
    - 1.1 Why MLOps, what are the considerations, high-level.
    - 1.2 MLOps tools overview.
  - Part 2: Hands-On.
    - 2.1: What is DVC.
    - 2.2: Live demo based on DVC Docs > Get started > Data Management.
    - 2.2.: Hands-On. Provide links to Get Started stuff for each tool.


## MLOps tools overview

Tools selection for the overview.

In the following, "MLOps" will be used interchangeably with terms ML automatization, ML pipelines.

For tools selection, define ML tooling sectors to exclude:

- AutoML
- Hyperparameter tuning
- Model serving (incl. deployment, monitoring)

From the references below, select FIVE tools max. Go by: 1) Currently most popular tools across the academically MLOps board (version controls, experiment trackers etc., not deployment etc.), as indicated by metrics: best-of-ml-python references, github topics. Other refs have lower prio. 2) Tools people in HDS-LEE actually use, as of current knowledge: Wandb, DVC, Hydra.

**Selection.**

- Selected: DVC, Hydra, Wandb, MLflow.
- Unsure: One of DataLad, Ploomber.
- Ignored: DataLad, Tensorboard, SageMaker, SnakeMake, Metaflow, ClearML, aim, Neptune, CometML, DagsHub, PolyAxon, AirFlow, KubeFlow, Ploomber, sacred, guild.ai.

**Selection reference lists.**

Popularity ranking lists.

- [best-of-ml-python - experiment-tracking](https://github.com/ml-tooling/best-of-ml-python#workflow--experiment-tracking) (ranked by GitHub src code popularity). 1 mlflow, 2 tensorboard, 3 dvc, 4 wandb, 5 sagemaker, 6 pycaret, 7 snakemake, 8 metaflow, 9 clearml, 10 aim, etc.)
- github topics
  - [mlops](https://github.com/topics/mlops). jina, label-studio, nni, qdrant, great-expectations, kedro, dagster, wandb, metaflow, bentoml.
  - [experiment-tracking](https://github.com/topics/experiment-tracking). kedro, aim, mlrun.
  - [reproducibility](https://github.com/topics/reproducibility). dvc, wandb, sacred, catalyst, lightning-hydra-template, etc.
  - [pipelines](https://github.com/topics/pipelines)
  - [llmops](https://github.com/topics/llmops)

Non-ranking, comparing lists.

- [dagshub blog - ml experiment tracking tools 2021](https://dagshub.com/blog/how-to-compare-ml-experiment-tracking-tools-to-fit-your-data-science-workflow/). mlflow, tensorboard, dvc, clearml, guild.ai, kubeflow, neptune.ai, wandb, comet.ml, sagemaker, dagshub.
- [neptune.ai blog - experiment tracking tools 2021](https://neptune.ai/blog/dvc-alternatives-for-experiment-tracking). neptune, wandb, comet.ml, mlflow, verta.ai, kubeflow, polyaxon, sagemaker, guild.ai.
- [ml-ops.org - workflow mgmt tools](https://github.com/visenger/awesome-mlops#mlops-workflow-management).
- [ploomber blog - workflow mgmt tools 2021](https://ploomber.io/blog/survey/).
- [fullstackdeeplearning - lecture 2](https://fullstackdeeplearning.com/course/2022/lecture-2-development-infrastructure-and-tooling/)


## Hands-on - References

Collect references for "Get Started" and "Further references" to provide for the different tools (same as in Tools overview). For each tool, prefer its docs' "Get Started", if available.

:::success
**Template** :first_place_medal: [Get Started](https://google.com)
:::

:::spoiler **Template** Further references.
- Template
:::

## Hands-on

### Hands-on - DVC
